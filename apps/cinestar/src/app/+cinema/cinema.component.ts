import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
	selector: 'app-cinema',
	template: `
		<router-outlet></router-outlet>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaComponent implements OnInit {
	constructor() {
	}

	ngOnInit() {
	}
}
