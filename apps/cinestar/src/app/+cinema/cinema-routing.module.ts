import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CinemaShowsResolverService} from './cinema-shows-resolver.service';
import {CinemaShowsComponent} from './cinema-shows/cinema-shows.component';
import {CinemaComponent} from './cinema.component';

const routes: Routes = [
	{
		path: '',
		component: CinemaComponent,
		children: [
			{
				path: ':cinema',
				component: CinemaShowsComponent,
				resolve: {
					shows: CinemaShowsResolverService
				}
			}
		]
	},

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class CinemaRoutingModule {
}
