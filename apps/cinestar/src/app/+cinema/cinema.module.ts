import {NgModule} from '@angular/core';
import {SharedModule} from '../+shared';
import {CinemaDataService} from './cinema-data.service';
import {CinemaRoutingModule} from './cinema-routing.module';
import {CinemaSandboxService} from './cinema-sandbox.service';
import {CinemaShowComponent} from './cinema-show/cinema-show.component';
import {CinemaShowsListComponent} from './cinema-shows-list/cinema-shows-list.component';
import {CinemaShowsResolverService} from './cinema-shows-resolver.service';
import {CinemaShowsComponent} from './cinema-shows/cinema-shows.component';
import {CinemaComponent} from './cinema.component';
import { CinemaShowSummaryComponent } from './cinema-show/cinema-show-summary.component';

@NgModule({
  imports: [SharedModule, CinemaRoutingModule],
  declarations: [CinemaComponent, CinemaShowsComponent, CinemaShowsListComponent, CinemaShowComponent, CinemaShowSummaryComponent],
  providers: [CinemaSandboxService, CinemaShowsResolverService, CinemaDataService]
})
export class CinemaModule {}
