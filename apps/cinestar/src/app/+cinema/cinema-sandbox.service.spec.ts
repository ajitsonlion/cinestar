import {inject, TestBed} from '@angular/core/testing';

import {CinemaSandboxService} from './cinema-sandbox.service';

describe('CinemaSandboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CinemaSandboxService]
    });
  });

  it(
    'should be created',
    inject([CinemaSandboxService], (service: CinemaSandboxService) => {
      expect(service).toBeTruthy();
    })
  );
});
