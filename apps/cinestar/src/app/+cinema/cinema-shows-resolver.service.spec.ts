import {inject, TestBed} from '@angular/core/testing';

import {CinemaShowsResolverService} from './cinema-shows-resolver.service';

describe('CinemaShowsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CinemaShowsResolverService]
    });
  });

  it(
    'should be created',
    inject([CinemaShowsResolverService], (service: CinemaShowsResolverService) => {
      expect(service).toBeTruthy();
    })
  );
});
