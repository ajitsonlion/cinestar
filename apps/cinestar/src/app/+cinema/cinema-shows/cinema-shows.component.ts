import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Data} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {debounceTime, distinctUntilChanged, map, pluck, startWith} from 'rxjs/operators';
import {Show} from '../cinema-data-model';
import {CinemaSandboxService} from '../cinema-sandbox.service';

@Component({
	selector: 'app-cinema-shows',
	templateUrl: './cinema-shows.component.html',
	styleUrls: ['./cinema-shows.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaShowsComponent implements OnInit {
	cinemaShows$: Observable<Show[]>;
	filterShow$: Observable<Show[]>;

	cinemaCtrl: FormControl;

	constructor(private readonly route: ActivatedRoute, private readonly sandbox: CinemaSandboxService) {
		this.cinemaShows$ = this.route.data.pipe(pluck<Data, Show[]>('cinemaShows'));

		this.cinemaCtrl = new FormControl();
		this.filterShow$ = this.cinemaCtrl.valueChanges.pipe(
			startWith(''),
			debounceTime(300),
			distinctUntilChanged(),
			map(show => this.filterShow(show))
		);
	}

	ngOnInit() {
	}

	private filterShow(show: string): Show[] {
		return this.sandbox.filter(show);
	}
}
