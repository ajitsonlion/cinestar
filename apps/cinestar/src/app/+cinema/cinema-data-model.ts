export interface Show {
  srcSet: ImgSet[];
  id: number;
  cinema: number;
  title: string;
  poster: string;
  poster_preload: string;
  movie?: number;
  trailer?: number;
  detailLink: string;
}

export interface PreviewShow extends Show {
  datetime: string;
}

export interface NoTimeShow extends Show {
  _type: string;
  subtitle: string;
  hasTrailer: boolean;
  attributes: any[];
  showtimes: any[];
  date?: string;
  event: number;
  startDate?: string;
  endDate?: any;
}

export interface ImgSet {
  media: string;
  url: string;
}
