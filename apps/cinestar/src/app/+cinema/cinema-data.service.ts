import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NoTimeShow, PreviewShow} from './cinema-data-model';

@Injectable()
export class CinemaDataService {
  constructor(private readonly http: HttpClient) {}

  getPreviewShows(cinema: number): Observable<PreviewShow[]> {
    return this.http.get<PreviewShow[]>(`/api/cinema/${cinema}/preview/?appVersion=1.5`);
  }
  getNoTimeShows(cinema: number): Observable<NoTimeShow[]> {
    return this.http.get<NoTimeShow[]>(`/api/cinema/${cinema}/show/no-showtimes/?appVersion=1.5`);
  }
	getShows(cinema: number): Observable<NoTimeShow[]> {
		return this.http.get<NoTimeShow[]>(`/api/cinema/${cinema}/show/?appVersion=1.5`);
	}
}
