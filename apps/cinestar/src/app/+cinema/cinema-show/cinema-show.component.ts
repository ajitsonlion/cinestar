import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {NoTimeShow, PreviewShow} from '../cinema-data-model';

@Component({
	selector: 'app-cinema-show',
	template: `
		<mat-card>
			<mat-card-header>
				<mat-card-title>{{show.title}}</mat-card-title>
			</mat-card-header>
			<mat-card-content>
				<app-show-info [show]="show">
					
				</app-show-info>
				
			</mat-card-content>

		</mat-card>
	`,
	styles: [`
	
	`],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaShowComponent implements OnInit, AfterViewInit {
	@Input() show: PreviewShow | NoTimeShow;

	constructor(private cd: ChangeDetectorRef) {
	}

	ngOnInit(): void {
	}

	ngAfterViewInit(): void {
		this.cd.detach();
	}
}
