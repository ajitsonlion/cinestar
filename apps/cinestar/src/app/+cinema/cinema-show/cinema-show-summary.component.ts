import {Component, OnInit, ChangeDetectionStrategy, Input} from '@angular/core';
import {NoTimeShow, PreviewShow} from '../cinema-data-model';

@Component({
  selector: 'app-show-info',
  templateUrl: './cinema-show-summary.component.html',
  styleUrls: ['./cinema-show-summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaShowSummaryComponent implements OnInit {
	@Input() show: PreviewShow | NoTimeShow;
	offset = 100;

  constructor() { }

  ngOnInit() {
  }

}
