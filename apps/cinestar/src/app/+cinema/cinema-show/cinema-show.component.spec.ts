import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CinemaShowComponent} from './cinema-show.component';

describe('CinemaShowComponent', () => {
  let component: CinemaShowComponent;
  let fixture: ComponentFixture<CinemaShowComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CinemaShowComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
