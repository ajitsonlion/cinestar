import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaShowSummaryComponent } from './cinema-show-summary.component';

describe('CinemaShowSummaryComponent', () => {
  let component: CinemaShowSummaryComponent;
  let fixture: ComponentFixture<CinemaShowSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaShowSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaShowSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
