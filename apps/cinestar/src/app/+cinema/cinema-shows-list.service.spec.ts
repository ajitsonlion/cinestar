import {inject, TestBed} from '@angular/core/testing';

import {CinemaShowsListService} from './cinema-shows-list.service';

describe('CinemaShowsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CinemaShowsListService]
    });
  });

  it(
    'should be created',
    inject([CinemaShowsListService], (service: CinemaShowsListService) => {
      expect(service).toBeTruthy();
    })
  );
});
