import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as Fuse from 'fuse.js';
import {FuseOptions} from 'fuse.js';
import {Memoize} from 'lodash-decorators';
import {Observable} from 'rxjs/Observable';
import {forkJoin} from 'rxjs/observable/forkJoin';
import {map, tap} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Subscription} from 'rxjs/Subscription';
import {ImgSet, NoTimeShow, PreviewShow, Show} from './cinema-data-model';
import {CinemaDataService} from './cinema-data.service';

@Injectable()
export class CinemaSandboxService implements OnDestroy {
	private options: FuseOptions = {
		shouldSort: true,
		tokenize: true,
		matchAllTokens: true,
		findAllMatches: true,
		threshold: 0.2,
		location: 0,
		distance: 100,
		maxPatternLength: 32,
		keys: ['title']
	};
	private _shows$ = new ReplaySubject<Show[]>(1);

	private $getShows: Subscription;

	private fuse: Fuse;
	private _shows: Show[] = [];
	responsive = new Map<string, string>();

	constructor(private readonly cinemaDataService: CinemaDataService, private router: Router) {
		this.responsive.set('(min-width: 420px)', 'web_m');
		this.responsive.set('(min-width: 992px)', 'web_x1');

		this.responsive.set('(min-width: 768px)', 'web_1');
	}

	getShows(cinema: number): Observable<Show[]> {
		return forkJoin(
			this.cinemaDataService.getPreviewShows(cinema),
			this.cinemaDataService.getNoTimeShows(cinema),
			this.cinemaDataService.getShows(cinema)
		).pipe(
			map((d: [PreviewShow[], NoTimeShow[]]) => [...d[0], ...d[1], ...d[2]]),
			map((d: Show[]) => this.computeShowsPosters(d)),
			tap((d: Show[]) => {
				this._shows$.next(d);
				this._shows = d;
				this.fuse = new Fuse(d, this.options);
			})
		);
	}

	ngOnDestroy(): void {
		if (this.$getShows) {
			this.$getShows.unsubscribe();
		}
	}

	filter(name: string): Show[] {
		if (!name) {
			return this._shows;
		}

		return this.fuse.search<Show>(name);
	}

	@Memoize()
	private computeSrcSet(url: string) {
		const imgSets: ImgSet[] = [];

		this.responsive.forEach((v, k) => {
			imgSets.push({
				media: k,
				url: url.replace(/preload/g, v)
			});
		});
		return imgSets;
	}

	private computeShowsPosters(shows: Show[]) {
		return shows.map(d => {
			d.srcSet = this.computeSrcSet(d.poster);
			return d;
		});
	}

	openShow(show: Show, route: ActivatedRoute) {
		this.router.navigate(['./show', show.id]);
	}
}
