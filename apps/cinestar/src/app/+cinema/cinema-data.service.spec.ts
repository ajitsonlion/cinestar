import {inject, TestBed} from '@angular/core/testing';

import {CinemaDataService} from './cinema-data.service';

describe('CinemaDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CinemaDataService]
    });
  });

  it(
    'should be created',
    inject([CinemaDataService], (service: CinemaDataService) => {
      expect(service).toBeTruthy();
    })
  );
});
