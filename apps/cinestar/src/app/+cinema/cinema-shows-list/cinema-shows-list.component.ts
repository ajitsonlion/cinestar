import {AfterViewInit, ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Show} from '../cinema-data-model';
import {CinemaSandboxService} from '../cinema-sandbox.service';

@Component({
	selector: 'app-cinema-shows-list',
	templateUrl: './cinema-shows-list.component.html',
	styleUrls: ['./cinema-shows-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaShowsListComponent implements OnInit, AfterViewInit {
	@Input() cinemaShows: Show[] = [];

	ngAfterViewInit(): void {
	}


	constructor(private sandbox: CinemaSandboxService,
							private route: ActivatedRoute) {
	}

	ngOnInit() {
		console.log(this.cinemaShows);
	}

	openShow(show: Show) {

		this.sandbox.openShow(show, this.route);
	}
}
