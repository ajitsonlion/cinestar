import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CinemaShowsListComponent} from './cinema-shows-list.component';

describe('CinemaShowsListComponent', () => {
  let component: CinemaShowsListComponent;
  let fixture: ComponentFixture<CinemaShowsListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CinemaShowsListComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaShowsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
