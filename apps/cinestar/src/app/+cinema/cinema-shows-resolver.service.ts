import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Show} from './cinema-data-model';
import {CinemaSandboxService} from './cinema-sandbox.service';

@Injectable()
export class CinemaShowsResolverService implements Resolve<Show[]> {
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Show[]> | Promise<Show[]> {
		return this.cinemaSandbox.getShows(route.params.cinema);
	}

	constructor(private cinemaSandbox: CinemaSandboxService) {
	}
}
