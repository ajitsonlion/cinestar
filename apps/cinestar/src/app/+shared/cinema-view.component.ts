import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-cinema-view',
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title>{{cinema.name}}</mat-card-title>
      </mat-card-header>
      <mat-card-content>

        <h3> {{cinema.shortName}}
        </h3>
        <h4> {{cinema.city}}</h4>

      </mat-card-content>
    </mat-card>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CinemaViewComponent {
  @Input() cinema: Cinema;
}
