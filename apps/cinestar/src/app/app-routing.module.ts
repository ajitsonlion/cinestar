import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
	{
		path: '',
		loadChildren: './+main/main.module#MainModule'
	},
	{
		path: 'cinema',
		loadChildren: './+cinema/cinema.module#CinemaModule'
	},
	{
		path: 'show',
		loadChildren: './+show/show.module#ShowModule'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules})],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
