import {NgModule} from '@angular/core';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {CoreModule} from './+core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';

@NgModule({
	declarations: [AppComponent],
	imports: [CoreModule, AppRoutingModule,
		ServiceWorkerModule.register('/ngsw-worker.js', {
			enabled: environment.production
		})],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
