import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShowInfoComponent} from './show-info.component';
import {ShowResolverService} from './show-resolver.service';
import {ShowComponent} from './show.component';

const routes: Routes = [
	{
		path: '',
		component: ShowComponent,
		children: [
			{
				path: ':show',
				component: ShowInfoComponent,
				resolve: {
					info: ShowResolverService
				}
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ShowRoutingModule {
}
