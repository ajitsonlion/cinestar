import { TestBed, inject } from '@angular/core/testing';

import { ShowSandboxService } from './show-sandbox.service';

describe('ShowSandboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowSandboxService]
    });
  });

  it('should be created', inject([ShowSandboxService], (service: ShowSandboxService) => {
    expect(service).toBeTruthy();
  }));
});
