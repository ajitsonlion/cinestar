import {NgModule} from '@angular/core';
import {SharedModule} from '../+shared';
import {ShowDataService} from './show-data.service';
import {ShowInfoComponent} from './show-info.component';
import {ShowResolverService} from './show-resolver.service';
import {ShowRoutingModule} from './show-routing.module';
import {ShowSandboxService} from './show-sandbox.service';
import {ShowComponent} from './show.component';

@NgModule({
	imports: [
		SharedModule,
		ShowRoutingModule
	],
	declarations: [ShowComponent, ShowInfoComponent],
	providers: [ShowResolverService, ShowSandboxService, ShowDataService]
})
export class ShowModule {
}
