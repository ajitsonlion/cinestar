import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ShowInfo} from './show-data.model';
import {ShowSandboxService} from './show-sandbox.service';

@Injectable()
export class ShowResolverService implements Resolve<ShowInfo> {
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShowInfo> | Promise<ShowInfo> | ShowInfo {
		return this.sandbox.getShowInfo(route.params.show);
	}

	constructor(private sandbox: ShowSandboxService) {
	}

}
