import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
	selector: 'app-show',
	template: `
		<router-outlet></router-outlet>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShowComponent implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}

}
