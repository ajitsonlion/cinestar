import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ShowInfo} from './show-data.model';
import {ShowDataService} from './show-data.service';

@Injectable()
export class ShowSandboxService {

	constructor(private dataService: ShowDataService) {
	}

	getShowInfo(show: number): Observable<ShowInfo> {
		return this.dataService.getShowInfo(show);

	}

}
