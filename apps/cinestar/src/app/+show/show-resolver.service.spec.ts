import { TestBed, inject } from '@angular/core/testing';

import { ShowResolverService } from './show-resolver.service';

describe('ShowResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowResolverService]
    });
  });

  it('should be created', inject([ShowResolverService], (service: ShowResolverService) => {
    expect(service).toBeTruthy();
  }));
});
