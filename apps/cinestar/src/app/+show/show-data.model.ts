export interface ShowInfo {
	_type: string;
	id: number;
	cinema: number;
	title: string;
	subtitle: string;
	hasTrailer: boolean;
	attributes: string[];
	fsk?: any;
	showtimes: Showtime[];
	poster_preload: string;
	poster: string;
	date: string;
	movie: number;
	trailer: number;
	detailLink: string;
}

export interface Showtime {
	id: number;
	name: string;
	cinema: number;
	datetime: string;
	emv: number;
	fsk: number;
	systemId: string;
	system: string;
	show: number;
	attributes: string[];
	screen: number;
}
