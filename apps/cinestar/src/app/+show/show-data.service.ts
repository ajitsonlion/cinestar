import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ShowInfo} from './show-data.model';

@Injectable()
export class ShowDataService {

	constructor(private http: HttpClient) {
	}

	getShowInfo(show: number): Observable<ShowInfo> {
		return this.http.get<ShowInfo>(`/api/show/${show}?appVersion=1.5`);

	}
}
