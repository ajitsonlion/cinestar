import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {pluck} from 'rxjs/operators';
import {ShowInfo} from './show-data.model';

@Component({
	selector: 'app-show-info',
	templateUrl: './show-info.component.html',
	styleUrls: ['./show-info.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShowInfoComponent implements OnInit {

	info$: Observable<ShowInfo>;

	constructor(private route: ActivatedRoute) {
		this.info$ = this.route.data.pipe(pluck<Data, ShowInfo>('info'));
	}

	ngOnInit() {
	}

}
