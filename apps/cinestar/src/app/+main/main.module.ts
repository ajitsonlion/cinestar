import {NgModule} from '@angular/core';
import {SharedModule} from '../+shared';
import {MainDataService} from './main-data.service';
import {MainResolverService} from './main-resolver.service';
import {MainRoutingModule} from './main-routing.module';
import {MainSandboxService} from './main-sandbox.service';
import {MainComponent} from './main.component';

@NgModule({
  imports: [SharedModule, MainRoutingModule],
  declarations: [MainComponent],
  providers: [MainResolverService, MainDataService, MainSandboxService]
})
export class MainModule {}
