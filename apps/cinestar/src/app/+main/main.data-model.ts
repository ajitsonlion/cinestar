interface Cinema {
  id: number;
  cinemaNumber: number;
  name: string;
  shortName: string;
  city: string;
  ticketing: string;
  ticketingLink: string;
  lat: number;
  lng: number;
  discountDay?: number;
  slug: string;
  nearByCinemas: number[];
}
