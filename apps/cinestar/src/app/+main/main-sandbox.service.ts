import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as Fuse from 'fuse.js';
import {FuseOptions} from 'fuse.js';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Subscription} from 'rxjs/Subscription';
import {MainDataService} from './main-data.service';

@Injectable()
export class MainSandboxService implements OnDestroy {
  private options: FuseOptions = {
    shouldSort: true,
    tokenize: true,
    matchAllTokens: true,
    findAllMatches: true,
    threshold: 0.2,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    keys: ['city', 'shortName']
  };

  private _cinema$ = new ReplaySubject<Cinema[]>(1);

  private $getCinemas: Subscription;

  private fuse: Fuse;
  private _cinemas: Cinema[] = [];

  constructor(private dataService: MainDataService, private router: Router) {
    this.$getCinemas = this.dataService.getCinemas().subscribe(d => {
      this._cinema$.next(d);
      this._cinemas = d;
      this.fuse = new Fuse(d, this.options);
    });
  }

  getCinemas() {
    return this._cinema$.asObservable();
  }

  ngOnDestroy(): void {
    if (this.$getCinemas) {
      this.$getCinemas.unsubscribe();
    }
  }

  filter(name: string) {
    if (!name) {
      return this._cinemas;
    }

    return this.fuse.search<Cinema>(name);
  }

  goToCinema(cinema: Cinema, route: ActivatedRoute) {
    this.router.navigate(['./cinema', cinema.id], { relativeTo: route });
  }
}
