import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {MainSandboxService} from './main-sandbox.service';

@Injectable()
export class MainResolverService implements Resolve<Cinema[]> {
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Cinema[]> | Promise<Cinema[]> {
		return this.sandbox.getCinemas();
	}

	constructor(private sandbox: MainSandboxService) {
	}
}
