import {inject, TestBed} from '@angular/core/testing';

import {MainResolverService} from './main-resolver.service';

describe('MainResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainResolverService]
    });
  });

  it(
    'should be created',
    inject([MainResolverService], (service: MainResolverService) => {
      expect(service).toBeTruthy();
    })
  );
});
