import {inject, TestBed} from '@angular/core/testing';

import {MainSandboxService} from './main-sandbox.service';

describe('MainSandboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainSandboxService]
    });
  });

  it(
    'should be created',
    inject([MainSandboxService], (service: MainSandboxService) => {
      expect(service).toBeTruthy();
    })
  );
});
