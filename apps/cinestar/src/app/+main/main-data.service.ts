import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class MainDataService {
	constructor(private http: HttpClient) {
	}

	getCinemas(): Observable<Cinema[]> {
		return this.http.get<Cinema[]>('/api/cinema/?appVersion=1.5');
	}
}
