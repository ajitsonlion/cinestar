import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {debounceTime, distinctUntilChanged, map, startWith} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';
import {MainSandboxService} from './main-sandbox.service';

@Component({
  selector: 'app-home',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit, OnDestroy {
  cinemas: Cinema[];
  cinemaCtrl: FormControl;
  filterCinema$: Observable<Cinema[]>;
  private $getCinemas: Subscription;

  constructor(private sandbox: MainSandboxService, private route: ActivatedRoute, private cd: ChangeDetectorRef) {
    this.cinemaCtrl = new FormControl();
    this.filterCinema$ = this.cinemaCtrl.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      distinctUntilChanged(),
      map(state => this.filterCinema(state))
    );
  }

  ngOnInit() {
    this.$getCinemas = this.sandbox.getCinemas().subscribe(d => {
      this.cinemas = d;
      this.cd.detectChanges();
    });
  }

  filterCinema(name: string): Cinema[] {
    return this.sandbox.filter(name);
  }

  goTo(cinema: Cinema) {
    this.sandbox.goToCinema(cinema, this.route);
  }

  ngOnDestroy(): void {
    this.$getCinemas.unsubscribe();
  }
}
