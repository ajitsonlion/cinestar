import {HttpClientModule} from '@angular/common/http';
/* 3rd party libraries */
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule
	],
	declarations: [],
	providers: [
		/* our own custom services  */
	]
})
export class CoreModule {
	/* make sure CoreModule is imported only by one NgModule the AppModule */
	constructor(@Optional()
							@SkipSelf()
								parentModule: CoreModule) {
		if (parentModule) {
			throw new Error('CoreModule is already loaded. Import only in AppModule');
		}
	}
}
